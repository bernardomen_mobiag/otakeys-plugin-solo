//
//  Environment.m
//  OtaKeysMvp
//
//  Created by Ricardo Silva on 06/02/2019.
//

#import "Environment.h"
#import "Constants.h"

@interface Environment ()
@property(nonatomic) enum EnvironmentPlugin environment;
@property(nonatomic) NSString *value;
@end

@implementation Environment

- (instancetype)initWithValue:(NSString *)value {
    self = [super init];
    if (self) {
        self.environment = [self getEnumEnvironmentFromString:value];
    }
    return self;
}

- (EnvironmentPlugin)getEnumEnvironmentFromString:(NSString *)value {
    _value = value;
    if ([value isEqualToString:kOTAEnvironmentDev]) {
        return DEV;
    } else if ([value isEqualToString:kOTAEnvironmentProd]) {
        return PROD;
    }
    return DEV;
}

- (NSString *)value {
    return _value.uppercaseString;
}

- (BOOL)isDev {
    return _environment == DEV;
}

#pragma mark - Auxiliary

+ (OTAEnvironment)getOTAEnumEnvironmentFromString:(NSString *)environment {
    if ([environment isEqualToString:@"SANDBOX"]) {
        return OTAEnvironmentSandbox;
    } else if ([environment isEqualToString:@"QA"]) {
        return OTAEnvironmentQA;
    } else if ([environment isEqualToString:@"VA"]) {
        return OTAEnvironmentVA;
    } else if ([environment isEqualToString:@"PRODUCTION"]) {
        return OTAEnvironmentProduction;
    }
    return OTAEnvironmentQA;
}

@end
