//
//  OtaKeysPlugin.h
//  OtaKeysMvp
//
//  Created by Ricardo Silva on 06/02/2019.
//

#import <Cordova/CDV.h>
#import <OTASDK/OTASDK.h>
#import "Constants.h"
#import "Environment.h"

@interface OtaKeysPlugin : CDVPlugin
@end
