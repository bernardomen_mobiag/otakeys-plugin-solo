//
//  Environment.h
//  OtaKeysMvp
//
//  Created by Ricardo Silva on 06/02/2019.
//

#import <OTASDK/OTAEnums.h>

@interface Environment : NSObject
- (instancetype)initWithValue:(NSString *)value;
- (NSString *)value;
- (BOOL)isDev;
+ (OTAEnvironment)getOTAEnumEnvironmentFromString:(NSString *)environment;
@end
