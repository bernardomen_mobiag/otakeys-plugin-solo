//
//  DataJSONObject.m
//  OtaKeysMvp
//
//  Created by Ricardo Silva on 06/02/2019.
//

#import "DataJSONObject.h"
#import "Constants.h"

@implementation DataJSONObject

#pragma mark - Success

+ (NSDictionary *)successObject:(NSString *)message {
    return [DataJSONObject successObject:message data:nil];
}

+ (NSDictionary *)successObject:(NSString *)message data:(id)data {
    NSString *finalMessage = [NSString stringWithFormat:@"%@%@",message,NotNilString(data)];
    return @{kOTAMessage:finalMessage};
}

+ (NSDictionary *)connected:(BOOL )connected successObject:(NSString *)message data:(id)data {
    NSString *finalMessage = [NSString stringWithFormat:@"%@%@",message,NotNilString(data)];
    return @{kOTAConnected:@(connected),
            kOTAMessage:finalMessage};
}

#pragma mark - Error

+ (NSDictionary *)errorObject:(NSString *)message {
    return [DataJSONObject errorObject:message data:nil];
}

+ (NSDictionary *)errorObject:(NSString *)message data:(id)data {
    NSString *finalMessage = [NSString stringWithFormat:@"Error : %@%@",message,NotNilString(data)];
    return @{kOTAMessage:finalMessage};
}

+ (NSDictionary *)errorObject:(NSString *)message code:(id)code {
    NSString *finalMessage = [NSString stringWithFormat:@"Error with code %@ - %@",code,message];
    return @{kOTAMessage:finalMessage};
}

#pragma mark - Session

+ (NSDictionary *)authenticatedObject:(NSString *)authenticated {
    return @{kOTAAuthenticated:authenticated};
}

+ (NSDictionary *)tokenObject:(NSString *)message token:(NSString *)token {
    return @{kOTAMessage:message,
             kOTAToken: token};
}

#pragma mark - Keys

+ (NSDictionary *)keyObject:(NSString *)message key:(OTAKeyPublic *)key {
    NSDictionary *otaKey = @{kOTAKeyPublicOtaId: NotNilString(key.otaId)};
    return @{kOTAMessage:message,
             kOTAKey: otaKey};
}

+ (NSDictionary *)keysObject:(NSString *)message keys:(NSArray<OTAKeyPublic *> *)keys {
    if (keys.count == 0) {
        return @{};
    }
    NSMutableArray *result = [NSMutableArray new];
    [keys enumerateObjectsUsingBlock:^(OTAKeyPublic * _Nonnull otaKey, NSUInteger idx, BOOL * _Nonnull stop) {
        [result addObject:@{kOTAKeyPublicOtaId: NotNilString(otaKey.otaId),
                            kOTAKeyPublicEnabled: @(otaKey.enabled),
                            kOTAKeyPublicBeginDate: @{
                                kOTAKeyPublicIMillis: [NSString stringWithFormat:@"%f",[otaKey.beginDate timeIntervalSince1970] * 1000]
                            },
                            kOTAKeyPublicEndDate: @{
                                kOTAKeyPublicIMillis: [NSString stringWithFormat:@"%f",[otaKey.endDate timeIntervalSince1970] * 1000]
                            },
                            kOTAKeyPublicVehicle: @{
                                kOTAVehiclePublicPlate: NotNilString(otaKey.vehicle.plate)
                            }}];
    }];
    
    return @{kOTAMessage:message,
             kOTAKeys: result};
}

#pragma mark - Vehicle

+ (NSDictionary *)vehicleObject:(NSString *)message vehicle:(OTAVehicleData *)vehicle {
    
    NSString *doors = @"UNKNOWN";
                if (vehicle.doorsState == 1) {
                    doors = @"LOCKED";
                }
                if (vehicle.doorsState == 3) {
                    doors = @"ALL_UNLOCKED";
                }
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init]; 
    [dateFormatter setDateFormat:@"MMM dd, yyyy HH:mm:ss"];



    NSString *lastCaptureDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate: vehicle.date]];
    NSString *lastEnergyCaptureDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate: vehicle.date]];
    NSString *lastGpsCaptureDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:vehicle.gpsCaptureDate]];
    NSString *lastMileageCaptureDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate: vehicle.date]];

    NSDictionary *vehicleKey = @{@"batteryVoltage":vehicle.batteryVoltage,
                                 @"connectedToCharger":@(vehicle.connectedToLoader),
                                 @"doorsState": doors,
                                 @"energyLevel": vehicle.energyStart,
                                 @"engineRunning": @(vehicle.engineRunning),
                                 @"gpsLatitude": vehicle.gpsLatitude,
                                 @"gpsLongitude": vehicle.gpsLongitude,
                                 @"lastCaptureDate": lastCaptureDateStr,
                                 @"lastEnergyCaptureDate": lastEnergyCaptureDateStr,
                                 @"lastGpsCaptureDate": lastGpsCaptureDateStr,
                                 @"lastMileageCaptureDate": lastMileageCaptureDateStr,
                                 @"mileage": NotNilString(vehicle.mileageStart)};
    return @{kOTAMessage:message,
             kOTAData: vehicleKey};
}


@end
