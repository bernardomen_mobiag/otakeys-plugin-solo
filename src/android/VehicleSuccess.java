package io.ptech.otakeys;

import com.otakeys.sdk.service.object.response.OtaVehicleData;

public class VehicleSuccess extends JObject {

    public VehicleSuccess(String message, OtaVehicleData data) {
        this.message = message;
        this.data = data;
    }

    String message;

    OtaVehicleData data;
}
