package io.ptech.otakeys;

import com.google.gson.Gson;

public class TokenRequest {

    public TokenRequest(String accessDeviceToken, String extId) {
        this.accessDeviceToken = accessDeviceToken;
        this.extId = extId;
    }

    String accessDeviceToken;

    String extId;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
