package io.ptech.otakeys;

public class ConnectedSuccess extends Success {

    public ConnectedSuccess(String message, boolean connected) {
        super(message);
        this.connected = connected;
    }

    boolean connected;
}
