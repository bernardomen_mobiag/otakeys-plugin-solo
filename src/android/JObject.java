package io.ptech.otakeys;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

public class JObject {

    public JSONObject toObject() {
        try {
            return new JSONObject(new Gson().toJson(this));
        } catch (JSONException e) {
            return null;
        }
    }
}
