package io.ptech.otakeys;

import com.otakeys.sdk.service.object.response.OtaLastVehicleData;

public class LastVehicleSuccess extends JObject {

    public LastVehicleSuccess(String message, OtaLastVehicleData data) {
        this.message = message;
        this.data = data;
    }

    String message;

    OtaLastVehicleData data;
}
