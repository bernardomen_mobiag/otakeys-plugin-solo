package io.ptech.otakeys;

import com.otakeys.sdk.service.object.response.OtaKey;

public class KeySuccess extends JObject {

    public KeySuccess(String message, OtaKey key) {
        this.message = message;
        this.key = key;
    }

    String message;

    OtaKey key;
}
