package io.ptech.otakeys;

public enum Environment {
    DEV("dev"),
    PROD("prod");

    private String value;

    Environment(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Environment fromString(String value) {
        if (DEV.getValue().equals(value)) {
            return DEV;
        }
        if (PROD.getValue().equals(value)) {
            return PROD;
        }
        throw new IllegalArgumentException("Unknown environment: " + value);
    }
}
