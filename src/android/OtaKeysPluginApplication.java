package io.ptech.otakeys;

import android.util.Log;

import com.otakeys.sdk.OtaKeysApplication;

public class OtaKeysPluginApplication extends OtaKeysApplication {

    @Override
    public void onCreate() {
        Log.d("MyApplication", "onCreate");
        super.onCreate();
    }
}
