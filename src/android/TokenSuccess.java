package io.ptech.otakeys;

public class TokenSuccess extends JObject {

    public TokenSuccess(String message, String token) {
        this.message = message;
        this.token = token;
    }

    String message;

    String token;
}
