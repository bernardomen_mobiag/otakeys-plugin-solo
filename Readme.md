# OTA Keys Cordova plugin MVP

## Requirements

[NodeJS](https://nodejs.org/en/) and [Cordova](https://cordova.apache.org/) need to be installed globally in the build machine.

After installing NodeJS [(instructions)](https://nodejs.org/en/download/package-manager/), to install Cordova in Unix systems run:

```bash
$ sudo npm install -g cordova
```

More info at [Cordova Website](https://cordova.apache.org/docs/en/2.9.0/guide/cli/index.html).

## Building the project

```bash
$ cd ota-keys-mvp-app
$ cordova requirements # Check if the build environment is ready
$ cordova build android # For Android
$ cordova build ios # For iOS
```

**Notes**

If an error occurs build for Android stating `No toolchains found in the NDK toolchains folder for ABI with prefix: mips64el-linux-android`, the run the following commands:

```bash
$ cd <path-to-ndk>
$ OS_=$(uname -s | tr [A-Z] [a-z])
$ mkdir -p toolchains/mipsel-linux-android-4.9/prebuilt/${OS_}-x86_64
$ touch toolchains/mipsel-linux-android-4.9/prebuilt/${OS_}-x86_64/NOTICE-MIPS
$ mkdir -p toolchains/mips64el-linux-android-4.9/prebuilt/${OS_}-x86_64
$ touch toolchains/mips64el-linux-android-4.9/prebuilt/${OS_}-x86_64/NOTICE-MIPS64
```

## Running the app

```bash
$ cordova emulate android # To run the app in an Android emulator
$ cordova emulate ios # To run the app in an iOS emulator
$ cordova run android # To run the app in an Android device
$ cordova run ios # To run the app in an iOS device
```

For more info see the [run](https://cordova.apache.org/docs/en/latest/reference/cordova-cli/index.html#cordova-run-command) and [emulate](https://cordova.apache.org/docs/en/latest/reference/cordova-cli/index.html#cordova-emulate-command) commands documentation.

## Manage the plugin

Before building place the native libraries in the `OtaKeysPlugin/native-libs/[android|ios]` directories.

The plugin should be reinstalled every time the plugin code changes.

```bash
$ cd ota-keys-mvp-app
$ cordova plugin remove cordova-plugin-otakeys # If plugin is already installed
$ cordova plugin add ../OtaKeysPlugin/
```

